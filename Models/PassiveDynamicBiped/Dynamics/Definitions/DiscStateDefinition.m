% *************************************************************************
% 
% function [discStateVec, discStateNames, discStateIndices] = DiscStateDefinition()
% function z = DiscStateDefinition()
%
% This MATLAB function defines the discrete state vector 'z' for a
% passive dynamic biped in 2D.  Besides serving as initial configuration of
% the model, this file provides a definition of the individual components
% of the discrete state vector and an index struct that allows name-based
% access to its values.
%
% NOTE: This function is relatively slow and should not be executed within
%       the simulation loop.
%
% Input:  - NONE
% Output: - The initial discrete states as the vector 'discStateVec' (or 'z')
%         - The corresponding state names in the cell array 'discStateNames' 
%         - The struct 'discStateIndices' that maps these names into indices  
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also HYBRIDDYNAMICS, FLOWMAP, JUMPMAP, JUMPSET, 
%            CONTSTATEDEFINITION, SYSTPARAMDEFINITION, 
%            VEC2STRUCT, STRUCT2VEC. 
%
function [discStateVec, discStateNames, discStateIndices] = DiscStateDefinition()
    
    % No discrete states:
    discStateVec = [];
    discStateNames = {};
    discStateIndices = struct([]);
end
